﻿AngularHelpers.Injectors.TmsClientCtrl = ["$scope", "$http"];

AngularHelpers.Constructors.TmsClientCtrl = function ($scope, $http) {

    $http.get("TmsClient/getClients").success(function (data) {
        var clientsArray = new Array();
        data.forEach(function (currentValue) {
            clientsArray.push(JSON.parse(currentValue));
        });
        $scope.clients = clientsArray;
        console.log(clientsArray);
    });

}

AngularHelpers.Constructors.TmsClientCtrl.$inject = AngularHelpers.Injectors.TmsClientCtrl;
angular.module("MyApp").controller("TmsClientCtrl", AngularHelpers.Constructors.TmsClientCtrl);

