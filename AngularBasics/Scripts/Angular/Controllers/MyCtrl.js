﻿AngularHelpers.Injectors.MyCtrl = ["$scope", "$http"];

AngularHelpers.Constructors.MyCtrl = function ($scope, $http) {

    $scope.model = {
        pageTitle: "TMS Client CRUD"
    }

};

AngularHelpers.Constructors.MyCtrl.$inject = AngularHelpers.Injectors.MyCtrl;
angular.module("MyApp").controller("MyCtrl", AngularHelpers.Constructors.MyCtrl);
