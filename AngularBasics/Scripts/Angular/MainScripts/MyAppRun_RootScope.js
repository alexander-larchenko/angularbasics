﻿
AngularHelpers.Injectors.MyAppRun = ["$rootScope"];

AngularHelpers.Constructors.MyAppRun = function ($rootScope) {

    $rootScope.name = 'TEST ROOTSCOPE';
    $rootScope.feature = 'TEST feature';
}

AngularHelpers.Constructors.MyAppRun.$inject = AngularHelpers.Injectors.MyAppRun;
angular.module("MyApp").run(AngularHelpers.Constructors.MyAppRun);