﻿//Create App

angular.module("MyApp", ["ngRoute"]);

//Config App

AngularHelpers.Injectors.MyAppConfig = ["$routeProvider"];

AngularHelpers.Constructors.MyAppConfig = function ($routeProvider) {

    $routeProvider.when('/TmsClients', {
        templateUrl: '/Scripts/Angular/Templates/TmsClient.html',
        controller: 'TmsClientCtrl'
    });

}

AngularHelpers.Constructors.MyAppConfig.$inject = AngularHelpers.Injectors.MyAppConfig;
angular.module("MyApp").config(AngularHelpers.Constructors.MyAppConfig);


