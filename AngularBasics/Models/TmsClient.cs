﻿using System;
using System.Globalization;

namespace AngularBasics.Models
{
    public class TmsClient
    {
        protected int PMSClientID;
        protected int AccessID;
        protected int ApplicationID;
        protected DateTime UpdateTS;
        protected String UpdatedBy;

        public TmsClient()
        {
        }

        public TmsClient(int pmsClientId, int accessId, int applicationId, DateTime updateTs, String updateBy)
        {
            PMSClientID = pmsClientId;
            AccessID = accessId;
            ApplicationID = applicationId;
            UpdateTS = updateTs;
            UpdatedBy = updateBy;
        }

        public string toJsonString()
        {
            return "{\"PMSClientID\":\"" + PMSClientID + "\","
                + "\"AccessID\":\"" + AccessID + "\","
                + "\"ApplicationID\":\"" + ApplicationID + "\","
                + "\"UpdateTS\":\"" + UpdateTS.ToString(CultureInfo.InvariantCulture) + "\","
                + "\"UpdatedBy\":\"" + UpdatedBy + "\""
                + "}";
        }
    }
}