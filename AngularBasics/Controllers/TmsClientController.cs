﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AngularBasics.Models;

namespace AngularBasics.Controllers
{
    public class TmsClientController : Controller
    {

        public string getClients()
        {
            List<string> tmsClientList = new List<string>();
            var jsonSerialiser = new JavaScriptSerializer();

            TmsClient client1 = new TmsClient(1, 1, 1, updateTs: DateTime.Now, updateBy: "no user");
            tmsClientList.Add(client1.toJsonString());

            TmsClient client2 = new TmsClient(2, 2, 2, updateTs: DateTime.Now, updateBy: "no user");
            tmsClientList.Add(client2.toJsonString());

            TmsClient client3 = new TmsClient(3, 3, 3, updateTs: DateTime.Now, updateBy: "no user");
            tmsClientList.Add(client3.toJsonString());

            return jsonSerialiser.Serialize(tmsClientList);
        }
    }
}
